public class Card{
	private String suit;
	private String value;

	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " of " + this.suit;
	}
	//Calculates the value of the cards as points
	public double calculatePoints(){
		double pointsEarned = 0.0;
		int valueNum = Integer.parseInt(this.value);
		
		if(this.suit == "Hearts"){
			pointsEarned += valueNum + 0.4;
		}
		else if(this.suit == "Diamonds"){
			pointsEarned += valueNum + 0.3;
		}
		else if(this.suit == "Clubs"){
			pointsEarned += valueNum + 0.2;
		}
		else if(this.suit == "Spades"){
			pointsEarned += valueNum + 0.1;
		}
		return pointsEarned;
	}	
}