public class SimpleWar{
	public static void main(String[] arg){
		Deck warDeck = new Deck();
		warDeck.shuffle();
		double cardOneScore = 0;
		double cardTwoScore = 0;
		while(warDeck.length() > 0){
			Card playerOneCard = warDeck.drawTopCard();
			Card playerTwoCard = warDeck.drawTopCard();
			cardOneScore = playerOneCard.calculatePoints();
			cardTwoScore = playerTwoCard.calculatePoints();
			System.out.println("------------------------------------------------------------------------\n" + "Player 1's Card: " + playerOneCard + "Score: " + cardOneScore +"\n");
			System.out.println("Player 2's Card: " + playerTwoCard + "Score: " + cardTwoScore);
			
		}
		//Prints congrats message depending on whether who wins
		if(cardOneScore > cardTwoScore){
				System.out.println("Player 1 wins, good job");
			}
			else if(cardOneScore < cardTwoScore){
				System.out.println("Player 2 wins, good job");
			}
			else{
				System.out.println("Its a tie");
			}
		
	}
}